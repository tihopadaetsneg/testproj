package ChangeFileName;

import java.io.File;
import java.io.FilenameFilter;


public class Test {
    public static void main(String[] args) {
        String dir = "C:\\Users\\user\\Documents\\2022oct\\2022oct";
        String ext = ".102";
        File[] listFiles = findFiles(dir, ext);
        for (File file: listFiles) {
            if (file.getName().endsWith(".102")){
                StringBuilder builder = new StringBuilder(file.getName());
                int start = file.getName().length() - 3;
                int end = file.getName().length();
                builder.replace(start, end, "zip");
                String temp = dir + "\\" + builder;
                file.renameTo(new File(temp));

            }
        }
    }

    private static File[] findFiles(String dir, String ext) {
        File file = new File(dir);
        if (!file.exists()) {
            System.out.println(dir + " папка не существует");
        }
        File[] listFiles = file.listFiles(new MyFileNameFilter(ext));
        return listFiles;
    }
    public static class MyFileNameFilter implements FilenameFilter {

        private String ext;

        public MyFileNameFilter(String ext){
            this.ext = ext.toLowerCase();
        }
        @Override
        public boolean accept(File dir, String name) {
            return name.toLowerCase().endsWith(ext);
        }
    }
}
