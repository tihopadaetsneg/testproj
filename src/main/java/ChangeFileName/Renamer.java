package ChangeFileName;
import java.io.*;
import java.util.*;

public class Renamer {
    public static void main(String[] arg) {
        switch (arg.length) {
            case 2:
                new Renamer(arg[0], arg[1], "./");
                break;
            case 3:
                if (!arg[2].endsWith(File.separator)) arg[2] += File.separator;
                new Renamer(arg[0], arg[1], arg[2]);
                break;
            default:
                outHelp();
        }
    }

    public static void outHelp() {
        System.out.println("Renamer usage:");
        System.out.println("\t java Renamer fileMask newFile path");
        System.out.println("");
        System.out.println("\t fileMask - file mask, such as 'php' ");
        System.out.println("\t newFile - new file name. 'NN' replaces with number");
        System.out.println("\t path - work path");
    }

    public List list(String fileMask, String path) {
        List rez = new ArrayList();
        String list[] = new File(path).list();
        if (list != null)
            for(String el : list)
                if (el.contains(fileMask))
                    rez.add(el);
        return rez;
    }

    public Renamer(String name, String newFileMask, String path) {
        int i = 0, ok = 0, fail = 0;
        for(Iterator it = list(name, path).iterator(); it.hasNext(); ) {
            String fileName = path.concat((String) it.next());
            String newFileName = path.concat(
                    newFileMask.replace("NN", Integer.toString(i++)));
            System.out.println(fileName + " => " + newFileName);
            try {
                new File(fileName).renameTo(new File(newFileName));
                ok++;
            } catch (Exception ex) {
                System.out.println("\tError rename: " + ex.toString());
            }
        }
        System.out.println("");
        System.out.println("Total " + ok + "/" + i + " files renamed");
    }
}
