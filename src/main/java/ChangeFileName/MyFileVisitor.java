package ChangeFileName;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class MyFileVisitor implements FileVisitor<Path> {
    private Path dir;
    private String ext;
    private int count = 0;

    public MyFileVisitor(Path dir, String ext) {
        this.dir = dir;
        this.ext = ext;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        String fileName = file.getFileName().toString();
        if (fileName.endsWith(".102")){
            System.out.println(fileName);
            count++;
            return FileVisitResult.CONTINUE;
        }else {
            System.out.println(count);
            return FileVisitResult.TERMINATE;
        }

    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return null;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return null;
    }
}
