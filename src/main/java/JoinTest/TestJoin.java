package JoinTest;

public class TestJoin {
    static class MyRunnable implements Runnable{
        private String name;
        @Override
        public void run() {
            for (int i = 0; i < 100000; i++) {
                System.out.println(name + " is run loop:"+i);
                try {
                    Thread.sleep(7);
                    Thread.yield();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public MyRunnable(String name) {
            this.name = name;
        }
    }
    public static void main(String[] args) throws InterruptedException {
        MyRunnable one = new MyRunnable("one");
        MyRunnable two = new MyRunnable("--two");
        Thread th1 = new Thread(one);
        Thread th2 = new Thread(two);
        System.out.println("one: " + th1.getState() + " | two: " + th2.getState());
        th1.start();
        th2.start();
        System.out.println("one: " + th1.getState() + " | two: " + th2.getState());
        th2.join();
        System.out.println("Вот тут мне интересно прервется ли или не прервется");
        System.out.println(55 + 55);
        System.out.println(one.name + " AAAAAAAAAAA");
//        th1.join();
        System.out.println("one: " + th1.getState() + " | two: " + th2.getState());
        System.out.println("Конец!");
    }
}
