package org.example;

public class Main {
    public static void main(String[] args) throws Exception {
        Clock clock = new Clock();
        Thread clockThread = new Thread(clock);
        clockThread.start();
//      clockThread.interrupt();
        Thread.sleep(10000);
        System.out.println(Thread.currentThread().getName() + " был остановлен на 10 секунд. Сейчас снова запустится");
        clockThread.interrupt();
    }
}


class Clock implements Runnable{

    @Override
    public void run() {
        Thread current = Thread.currentThread();
//        try {
//            Thread.sleep(50);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        while (!current.isInterrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
                current.interrupt();
            }
            System.out.println("Tik");
        }
    }
}